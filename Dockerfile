# Use the official Node.js image from the Docker Hub
FROM node:14

# Install Docker CLI
RUN apt-get update && apt-get install -y \
    docker.io \
    && rm -rf /var/lib/apt/lists/*
    
# Set build arguments for Docker registry credentials
ARG DOCKER_USER
ARG DOCKER_PASSWORD

# Set the working directory in the container
WORKDIR /usr/app

# Copy package.json and package-lock.json
COPY package*.json ./

USER node
# Install dependencies
RUN npm i
# RUN npm run build
# USER root
# RUN npm update -g
# RUN find  /usr/local/lib/node_modules -iname *.key | grep test | xargs rm -f  \
#    && find  /usr/local/lib/node_modules -iname key.pem | grep test | xargs rm -f

USER node
# Log in to Docker registry
# RUN echo $DOCKER_PASSWORD | docker login -u $DOCKER_USER --password-stdin $DOCKER_PASSWORD

# Copy the rest of your application code
COPY . .

# Expose the port the app runs on
EXPOSE 4000

# Define the command to run your app
CMD ["node", "app.js"]
